# Inotify script for jpg/pdf parsing

- on write pdf/jpg it will convert/ocr and sort into dirs depending on whats detected in ocr text
- needs tesseract,pdfsandwich, convert and inotify

> my scanner places scans automatically in INBOX

directory structure
.
 * [INBOX](./INBOX)
   * [scannerfile.jpg](./INBOX/scannerfile.jpg)
   * [scannerfile.pdf](./INBOX/scannerfile.pdf)
 * [verwerker](./verwerker)
   * [bin](./bin)
   * [runner.sh](./bin/runner.sh)
   * [ocr](./ocr)
   * [pdfsandwich](./pdfsandwich)
   * [insurance](./insurance)

