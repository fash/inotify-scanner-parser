#!/bin/bash
set -x
mypath=/mnt/private/BURN/scanner/verwerker/
cd $mypath
myfile=$1
mv ../INBOX/$1 ./

if [ $( echo $myfile | grep -i jpg$ ) ]; then
        echo $myfile > /tmp/orgname
        convert -density 300 $myfile -depth 8 /tmp/$myfile.tiff   
        mv /tmp/$myfile.tiff ../INBOX/
        exit 0
fi
if [ $( echo $myfile | grep -v tiff | grep -i pdf$   ) ]; then
        echo $myfile > /tmp/orgname
        pdfsandwich $myfile -o pdfsandwich/$myfile
        convert -density 300 $myfile -depth 8 /tmp/$myfile.tiff   
        mv /tmp/$myfile.tiff ../INBOX/
        find /tmp/pdfsandwich_inputfile* -type l -exec unlink {} \;
        exit 0
fi
if [ $( echo $myfile | grep -i tiff$ ) ]; then
        tesseract $myfile ocr/$myfile -l nld
        sed -i -e 's/\r*$/\r/' ocr/$myfile.txt
        rm -f $myfile

        grep -i insurance ocr/$myfile.txt && insurance=1
        if [ "w$insurance" == "w1" ] ; then
                myfile=$(cat /tmp/orgname)
                mv $myfile insurance ; mv ocr/$myfile.tiff.txt insurance/$myfile.txt ; mv pdfsandwich/$myfile insurance/ocr_$myfile 
        fi
fi

